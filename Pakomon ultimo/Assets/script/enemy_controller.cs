﻿using UnityEngine;
using System.Collections;

public class enemy_controller : MonoBehaviour {

    public GameObject starpoint;
    public GameObject endPoint;
    public float enemySpeed;
    private bool isGoingRight;

	// Use this for initialization
	void Start () {
        if(!isGoingRight)
        {
            transform.position = starpoint.transform.position;
        }
        else
        {
            transform.position = endPoint.transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!isGoingRight)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPoint.transform.position, enemySpeed * Time.deltaTime);
            if (transform.position == endPoint.transform.position)
            {
                isGoingRight = true;
                GetComponent<SpriteRenderer>().flipX = true;
            }
        }
        if (isGoingRight)
        {
            transform.position = Vector3.MoveTowards(transform.position, starpoint.transform.position, enemySpeed * Time.deltaTime);
            if (transform.position == starpoint.transform.position)
            {
                isGoingRight = false;
                GetComponent<SpriteRenderer>().flipX = false;
            }
        }
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.CompareTag("Player"))
        {
            Destroy(c.gameObject);
        }
    }
}
