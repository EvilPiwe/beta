﻿using UnityEngine;
using System.Collections;

public class MovientoTrayectoria : MonoBehaviour {

    public Transform centro;
    private float x0, y0, x, y, r, angulo, tiempo;

	// Use this for initialization
	void Start () {
        r = 5f;
        angulo = Mathf.PI / 4;
        x0 = centro.transform.position.x;
        y0 = centro.transform.position.y;
        tiempo = 0f;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (tiempo >= 0.05f)
        {
            x = x0 + r * Mathf.Cos(angulo);
            y = y0 + r * Mathf.Sin(angulo);
            angulo = (angulo + Mathf.PI / 32) % (2*Mathf.PI);
            transform.localPosition = new Vector2(x, y);
            tiempo = 0f;
        }
        else
        {
            tiempo += Time.deltaTime;
        }
	}
}
