﻿using UnityEngine;
using System.Collections;

public class player_controller : MonoBehaviour {
    public float altura_salto;
    public float velocidad_movimiento;
    private Rigidbody2D rb;
    private Animator anim;
    private bool toco_piso;
    private Vector2 pos_o;
    public const string muerte = "muerte";

    // Use this for initialization
    void Start() {
        pos_o = this.transform.position;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        anim.SetInteger("estado",1);
	}
	
    void OnCollisionEnter2D(Collision2D c)
    {
       toco_piso = c.gameObject.tag.Equals("piso");


        if (c.transform.tag.Equals(muerte))
        {
            this.transform.position = pos_o;
        }
    }
    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.Space) && toco_piso)
        {
            rb.velocity = new Vector2(rb.velocity.x, altura_salto);
          //  anim.SetInteger("estado", 2);
            toco_piso = false;

        }
        if (Input.GetKey(KeyCode.Space) )
        {
           
            anim.SetInteger("estado", 2);
           

        }

        else
        {
            anim.SetInteger("estado", 1);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocidad_movimiento, rb.velocity.y);
            rb.transform.localScale = new Vector2(1, 1);

            if (!Input.GetKey(KeyCode.Space) && Input.GetKey(KeyCode.RightArrow)) {
                anim.SetInteger("estado", 1);
            }

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocidad_movimiento, rb.velocity.y);
            rb.transform.localScale = new Vector2(-1, 1);
            if (!Input.GetKey(KeyCode.Space) && Input.GetKey(KeyCode.LeftArrow))
            {
                anim.SetInteger("estado", 1);
            }
        }
        
    }
}
